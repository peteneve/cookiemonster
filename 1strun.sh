# Get top 1 million sites from Cisco Umbrella
wget http://s3-us-west-1.amazonaws.com/umbrella-static/top-1m.csv.zip
unzip top-1m.csv.zip
echo 'var linkArray = [' > linkArray.js
cat top-1m.csv | cut -f 2 -d "," | sed -e 's/^/\"http:\/\//' | sed -e 's/.$/\",/' >> linkArray.js
echo '];' >> linkArray.js

# Add the following to the ROOT crontab
# @reboot /home/pi/cookiemonster/startCookieMonster.sh &
# @reboot /home/pi/cookiemonster/status.py &
