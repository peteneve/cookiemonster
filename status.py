#!/usr/bin/python
import os
import sys
from blinkt import set_pixel, set_brightness, show, clear
import time

pidfile = "/tmp/cookiemonster.pid"
pidflag = 0

files = ['/home/pi/cookiemonster/cookies.log']
me = sys.argv[0]
i = 0

os.utime(me, None)

clear()
set_pixel(0, 255, 0, 0)
show()

while True:
    if os.path.isfile(pidfile) and pidflag == 0:
        clear()
        set_pixel(0, 0, 0, 255)
        show()
        pidflag = 1;
    mytime= os.path.getmtime(me)
    for f in files:
        if os.path.isfile(f):
            ft = os.path.getmtime(f)
            if ft > mytime:
                print f, "changed"
                i = i + 1
                if i > 7:
                    i = 0
                clear()
                set_pixel(i, 0, 255, 0)
                show()
                os.utime(me, None)
    time.sleep(0.1)
